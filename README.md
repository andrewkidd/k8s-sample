# k8s-sample

Dockerized TypeScript-Node-Starter with Azure Kubernetes deployment and GitLab managed Terraform state.

 [![pipeline status](https://gitlab.com/andrewkidd/k8s-sample/badges/master/pipeline.svg)](http://k8s-sample.andrewkidd.co.uk) 

[Intended primarily as a self educational excercise, more fluff here.](https://andrewkidd.co.uk/blog/2022/01/23/Docker-Gitlab-Terraform-Kubernetes-Node/)

## Deployment
### GitLab CI/CD Requirements

1. [There are four required variables to define Azure Resource Manager credentials used by Terraform to create the kubernetes service](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/managed_service_identity#configuring-with-environment-variables). Set the following under Project > Settings > CI/CD > Variables 
   - ARM_CLIENT_ID: xxxx
   - ARM_CLIENT_SECRET: xxxx
   - ARM_SUBSCRIPTION_ID: xxxx
   - ARM_TENANT_ID: xxxx
2. [There is a special deploy token that is automatically exposed to CI/CD pipelines and gives access to the GitLab projects resources](https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token), in this case the Container Registry. Create this token under Project > Settings > Deploy Tokens
   - Name: gitlab-deploy-token
   - Scopes: 
     - read_registry

### Running Deployment
 - With the above requirements in place, a push to the master branch will kick off a successful deployment.
 - Navigate to [Azure Portal > Kubernetes Services](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.ContainerService%2FmanagedClusters) > k8s-sample-kubernetes-service-k8s > Services and Ingresses
 - App is serviced live on: nginxweb-udp > External IP

## Running Locally
### Workstation Requirements
 1. [Install Docker](https://docs.docker.com/get-docker/)
    1. (Optional) On Windows [install WSL](https://www.microsoft.com/en-gb/p/alpine-wsl/9p804crf0395)
 2. [Install VSCode](https://code.visualstudio.com/download)
 3. [Install A Git Client](https://desktop.github.com/)

### Running the app
 - Within VSCode, press CTRL+Shift+B and select `docker-compose-up-dev`
 - Alternatively use the Task Explorer explorer extension panel to browse all scripts.
 - App is served locally at http://localhost:80
 - Open Docker Desktop to view environment state interactively

# More Info

- [Microsoft TypeScript Node Starter](https://github.com/microsoft/TypeScript-Node-Starter)
  - [TypeScript Compile](https://www.typescriptlang.org/docs/handbook/compiler-options.html)
  - [Pug Templating Engine](https://github.com/pugjs/pug) 
- [GitLab CI/CD Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
  - [GitLab Managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
    - [GitLab Terraform Helper Image](https://gitlab.com/gitlab-org/terraform-images)
    - [GitLab Terraform Helper With Azure CLI](https://hub.docker.com/r/andrewkidd/gitlab-terraform-azure-cli)
  - [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
    - [GitLab Deploy Tokens](https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token)
- [Terraform AzureRM Provider](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs)
- [Docker Desktop](https://www.docker.com/products/docker-desktop)
  - [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)
  - [Docker Compose](https://docs.docker.com/compose/)
- [Azure Kubernetes Service](https://azure.microsoft.com/en-gb/services/kubernetes-service/)
  - [Kompose](https://github.com/kubernetes/kompose)
