FROM node:alpine
ENV WORKDIR=/home/node/app/
WORKDIR ${WORKDIR}

### ----------------------------------------------------------
### Prepare application
### ----------------------------------------------------------

COPY --chown=node:node ./src ./src
COPY --chown=node:node ./views ./views
COPY --chown=node:node ./*.json ./
COPY --chown=node:node ./copyStaticAssets.ts ./copyStaticAssets.ts
COPY --chown=node:node ./.eslintignore ./.eslintignore
COPY --chown=node:node ./.eslintrc ./.eslintrc
RUN npm ci
RUN npm run build
RUN find . -maxdepth 1 \! \( -name "package*.json" -o -name "views" -o -name "dist" -o -name "node_modules" \) | awk '!((/^.$/) || (/^..$/))' | xargs --no-run-if-empty rm -rf
#USER node

### ----------------------------------------------------------
### Prepare application entrypoint
### ----------------------------------------------------------

# Expose the port node is reachable on
EXPOSE 5000

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://nodeapp:5000 || exit 1   

# start node
CMD [ "npm", "run", "serve"]