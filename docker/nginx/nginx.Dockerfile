FROM nginx:alpine
ENV WORKDIR=/var/www/html
ENV HOME=${WORKDIR}
WORKDIR ${WORKDIR}

### ----------------------------------------------------------
### Copy prepared configuration files
### ----------------------------------------------------------

# Configure nginx
COPY docker/nginx/config/nginx-node.conf /etc/nginx/conf.d/default.conf

### ----------------------------------------------------------
### Prepare application entrypoint
### ----------------------------------------------------------

# Expose the port nginx is reachable on
EXPOSE 80

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://nginxweb:80 || exit 1   

# Start node
CMD ["nginx", "-g", "daemon off;"]