variable "app_name" {
  type = string
}

variable "app_resource_group_name" {
  type = string
}

variable "app_location" {
  type = string
}