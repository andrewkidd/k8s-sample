resource "azurerm_kubernetes_cluster" "app_lubernetes_service_nodes" {
  name                = "${var.app_name}-k8s"
  location            = var.app_location
  resource_group_name = var.app_resource_group_name
  dns_prefix          = "${var.app_name}-k8s"

  default_node_pool {
    name       = "agentpool"
    node_count = 1
    vm_size    = "Standard_B4ms"
  }

  identity {
    type = "SystemAssigned"
  }
}