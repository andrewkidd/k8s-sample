output "client_certificate" {
    value = module.kubernetes_service.client_certificate
    sensitive = true
}

output "kube_config" {
    value = module.kubernetes_service.kube_config
    sensitive = true
}