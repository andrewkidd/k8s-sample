variable "app_name" {
  type = string
  default = "k8s-sample"
  description = "name used for app related resources"
}

variable "app_location" {
  type = string
  default = "uksouth"
  description = "location for app related resources to be deployed to"
}