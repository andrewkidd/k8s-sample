# We strongly recommend using the required_providers block to set the
# Azure Provider source and version being used
terraform {
  backend "http" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}

# Configure any local variables
locals {
    app_name 		= var.app_name
	app_location 	= var.app_location
}

# Prepare app resource group
resource "azurerm_resource_group" "app_resource_group" {
  name     = "${local.app_name}-resource-group"
  location = local.app_location
}

# Load in kubernetes deploy module
module "kubernetes_service" {
  source                        = "./modules/k8s"
  app_name                      = "${local.app_name}-kubernetes-service"
  app_resource_group_name       = azurerm_resource_group.app_resource_group.name
  app_location                  = azurerm_resource_group.app_resource_group.location
}