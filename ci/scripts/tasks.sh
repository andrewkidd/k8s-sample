#!/bin/sh
set -e
# The purpose of this script is to parse and run vscode tasks by name during CI
TASKNAME=$1
ARGS="${@:2}"
CMD=$(cat ./.vscode/tasks.json | jq -r ".tasks | .[] | select(.label==\"$TASKNAME\") | .command")
eval echo "Executing Task '$TASKNAME': $CMD"
eval $CMD