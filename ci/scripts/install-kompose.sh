#!/bin/sh
set -e
#apk add kompose --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing
apk add curl
mkdir /usr/local/sbin/
curl -L https://github.com/kubernetes/kompose/releases/download/v1.26.0/kompose-linux-amd64 -o /usr/local/sbin/kompose
chmod +x /usr/local/sbin/kompose